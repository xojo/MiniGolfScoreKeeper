#tag IOSView
Begin iosView ScoreView
   BackButtonTitle =   ""
   Compatibility   =   ""
   Left            =   0
   NavigationBarVisible=   True
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   "Enter Scores"
   Top             =   0
   Begin iOSLabel P1Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P1Label, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P1Label, 1, <Parent>, 1, False, +1.00, 4, 1, *kStdGapCtlToViewH, , True
      AutoLayout      =   P1Label, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, *kStdControlGapV, , True
      AutoLayout      =   P1Label, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   20
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "Player 1:"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   73
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSTextField P1Field
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P1Field, 8, , 0, True, +1.00, 4, 1, 31, , True
      AutoLayout      =   P1Field, 1, P1Label, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P1Field, 3, P1Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P1Field, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   31.0
      KeyboardType    =   "4"
      Left            =   120
      LockedInPosition=   False
      Password        =   False
      PlaceHolder     =   ""
      Scope           =   0
      Text            =   ""
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   73
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSLabel P1TotalScoreLabel
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P1TotalScoreLabel, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P1TotalScoreLabel, 2, <Parent>, 2, False, +1.00, 4, 1, -*kStdGapCtlToViewH, , True
      AutoLayout      =   P1TotalScoreLabel, 3, P1Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P1TotalScoreLabel, 7, , 0, False, +1.00, 4, 1, 70, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   230
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "0"
      TextAlignment   =   "2"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   73
      Visible         =   True
      Width           =   70.0
   End
   Begin iOSLabel P2Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P2Label, 1, P1Label, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P2Label, 7, , 0, False, +1.00, 4, 1, 100, , True
      AutoLayout      =   P2Label, 3, P1TotalScoreLabel, 4, False, +1.00, 4, 1, *kStdControlGapV, , True
      AutoLayout      =   P2Label, 8, , 0, False, +1.00, 4, 1, 30, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   20
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "Player 2:"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   111
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSTextField P2Field
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P2Field, 8, , 0, True, +1.00, 4, 1, 31, , True
      AutoLayout      =   P2Field, 1, P2Label, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P2Field, 3, P2Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P2Field, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   31.0
      KeyboardType    =   "4"
      Left            =   120
      LockedInPosition=   False
      Password        =   False
      PlaceHolder     =   ""
      Scope           =   0
      Text            =   ""
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   111
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSLabel P2TotalScoreLabel
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P2TotalScoreLabel, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P2TotalScoreLabel, 2, <Parent>, 2, False, +1.00, 4, 1, -*kStdGapCtlToViewH, , True
      AutoLayout      =   P2TotalScoreLabel, 3, P2Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P2TotalScoreLabel, 7, , 0, False, +1.00, 4, 1, 70, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   230
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "0"
      TextAlignment   =   "2"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   111
      Visible         =   True
      Width           =   70.0
   End
   Begin iOSLabel P3Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P3Label, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P3Label, 1, P1Label, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P3Label, 3, P2Label, 4, False, +1.00, 4, 1, *kStdControlGapV, , True
      AutoLayout      =   P3Label, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   20
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "Player 3:"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   149
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSTextField P3Field
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P3Field, 8, , 0, True, +1.00, 4, 1, 31, , True
      AutoLayout      =   P3Field, 1, P3Label, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P3Field, 3, P3Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P3Field, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   31.0
      KeyboardType    =   "4"
      Left            =   120
      LockedInPosition=   False
      Password        =   False
      PlaceHolder     =   ""
      Scope           =   0
      Text            =   ""
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   149
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSLabel P3TotalScoreLabel
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P3TotalScoreLabel, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P3TotalScoreLabel, 2, <Parent>, 2, False, +1.00, 4, 1, -*kStdGapCtlToViewH, , True
      AutoLayout      =   P3TotalScoreLabel, 3, P3Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P3TotalScoreLabel, 7, , 0, False, +1.00, 4, 1, 70, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   230
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "0"
      TextAlignment   =   "2"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   149
      Visible         =   True
      Width           =   70.0
   End
   Begin iOSLabel P4Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P4Label, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P4Label, 1, P1Label, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P4Label, 3, P3Label, 4, False, +1.00, 4, 1, *kStdControlGapV, , True
      AutoLayout      =   P4Label, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   20
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "Player 4:"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   187
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSTextField P4Field
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P4Field, 8, , 0, True, +1.00, 4, 1, 31, , True
      AutoLayout      =   P4Field, 1, P4Label, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P4Field, 3, P4Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P4Field, 7, , 0, False, +1.00, 4, 1, 100, , True
      Enabled         =   True
      Height          =   31.0
      KeyboardType    =   "4"
      Left            =   120
      LockedInPosition=   False
      Password        =   False
      PlaceHolder     =   ""
      Scope           =   0
      Text            =   ""
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   187
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSLabel P4TotalScoreLabel
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   P4TotalScoreLabel, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   P4TotalScoreLabel, 2, <Parent>, 2, False, +1.00, 4, 1, -*kStdGapCtlToViewH, , True
      AutoLayout      =   P4TotalScoreLabel, 3, P4Label, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   P4TotalScoreLabel, 7, , 0, False, +1.00, 4, 1, 70, , True
      Enabled         =   True
      Height          =   30.0
      Left            =   230
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "0"
      TextAlignment   =   "2"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   187
      Visible         =   True
      Width           =   70.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Method, Flags = &h0
		Function CalculateScore(player As Integer) As Integer
		  // Player is 0-based
		  Dim totalScore As Integer
		  For i As Integer = 0 To 17
		    totalScore = totalScore + App.PlayerScores(player, i)
		  Next
		  
		  Return totalScore
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Save()
		  // Save scores to a text file
		  // Format is player number followed by scores for holes. Tab-delimited.
		  
		  Dim scoreFile As FolderItem = SpecialFolder.Documents.Child("Scores.txt")
		  
		  Dim output As TextOutputStream
		  output = TextOutputStream.Create(scoreFile, Xojo.Core.TextEncoding.UTF8)
		  
		  Dim tab As Text = &u09
		  
		  For player As Integer = 0 To 3
		    output.Write(player.ToText) + tab
		    For hole As Integer = 0 To 17
		      output.Write(App.PlayerScores(player, hole).ToText) + tab
		    Next
		    output.WriteLine("")
		  Next
		  
		  output.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowScores(hole As Integer)
		  CurrentHole = hole
		  
		  Dim displayHole As Integer = hole + 1
		  
		  Self.Title = "Enter Scores for Hole #" + displayHole.ToText
		  
		  P1Field.Text = App.PlayerScores(0, hole).ToText
		  P1TotalScoreLabel.Text = CalculateScore(0).ToText
		  
		  P2Field.Text = App.PlayerScores(1, hole).ToText
		  P2TotalScoreLabel.Text = CalculateScore(1).ToText
		  
		  P3Field.Text = App.PlayerScores(2, hole).ToText
		  P3TotalScoreLabel.Text = CalculateScore(2).ToText
		  
		  P4Field.Text = App.PlayerScores(3, hole).ToText
		  P4TotalScoreLabel.Text = CalculateScore(3).ToText
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CurrentHole As Integer
	#tag EndProperty


#tag EndWindowCode

#tag Events P1Field
	#tag Event
		Sub TextChange()
		  Dim player As Integer = 0 // 0-based, so a 0 indicates player 1
		  
		  Dim score As Integer = Integer.Parse(Me.Text)
		  
		  App.PlayerScores(player, CurrentHole) = score
		  
		  P1TotalScoreLabel.Text = CalculateScore(player).ToText
		  
		  Save
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events P2Field
	#tag Event
		Sub TextChange()
		  Dim player As Integer = 1 // 0-based, so a 1 indicates player 2
		  
		  Dim score As Integer = Integer.Parse(Me.Text)
		  
		  App.PlayerScores(player, CurrentHole) = score
		  
		  P2TotalScoreLabel.Text = CalculateScore(player).ToText
		  
		  Save
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events P3Field
	#tag Event
		Sub TextChange()
		  Dim player As Integer = 2 // 0-based, so a 2 indicates player 3
		  
		  Dim score As Integer = Integer.Parse(Me.Text)
		  
		  App.PlayerScores(player, CurrentHole) = score
		  
		  P3TotalScoreLabel.Text = CalculateScore(player).ToText
		  
		  Save
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events P4Field
	#tag Event
		Sub TextChange()
		  Dim player As Integer = 3 // 0-based, so a 3 indicates player 4
		  
		  Dim score As Integer = Integer.Parse(Me.Text)
		  
		  App.PlayerScores(player, CurrentHole) = score
		  
		  P4TotalScoreLabel.Text = CalculateScore(player).ToText
		  
		  Save
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentHole"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
