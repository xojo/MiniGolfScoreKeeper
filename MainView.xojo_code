#tag IOSView
Begin iosView MainView
   BackButtonTitle =   ""
   Compatibility   =   ""
   Left            =   0
   NavigationBarVisible=   True
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   "Mini-Golf Scores"
   Top             =   0
   Begin iOSTable CourseTable
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   CourseTable, 4, BottomLayoutGuide, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   CourseTable, 1, <Parent>, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   CourseTable, 2, <Parent>, 2, False, +1.00, 4, 1, -0, , True
      AutoLayout      =   CourseTable, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, 0, , True
      EditingEnabled  =   False
      EditingEnabled  =   False
      EstimatedRowHeight=   -1
      Format          =   "0"
      Height          =   415.0
      Left            =   0
      LockedInPosition=   False
      Scope           =   0
      SectionCount    =   0
      Top             =   65
      Visible         =   True
      Width           =   320.0
   End
   Begin iOSToolButton NewGameButton
      Caption         =   "New Game"
      Enabled         =   True
      Height          =   22
      Image           =   "0"
      Image           =   "0"
      Left            =   234
      LockedInPosition=   False
      Scope           =   0
      Top             =   32
      Type            =   "1001"
      Width           =   78.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Event
		Sub ToolbarPressed(button As iOSToolButton)
		  If button = NewGameButton Then
		    // Clear all scores
		    For player As Integer = 0 To 3
		      For hole As Integer = 0 To 17
		        App.PlayerScores(player, hole) = 0
		      Next
		    Next
		    
		  End If
		End Sub
	#tag EndEvent


#tag EndWindowCode

#tag Events CourseTable
	#tag Event
		Sub Open()
		  Me.AddSection("")
		  For i As Integer = 1 To 18
		    Dim cell As iOSTableCellData
		    cell = Me.CreateCell
		    cell.Text = "Hole " + i.ToText
		    cell.AccessoryType = iOSTableCellData.AccessoryTypes.Detail
		    Me.AddRow(0, cell)
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub AccessoryAction(section As Integer, row As Integer)
		  Dim v As New ScoreView
		  v.ShowScores(row)
		  Self.PushTo(v)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
