#tag Class
Protected Class App
Inherits IOSApplication
	#tag CompatibilityFlags = TargetIOS
	#tag Event
		Sub Open()
		  LoadScores
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub LoadScores()
		  // Load scores from saved file
		  
		  Dim scoreFile As FolderItem = SpecialFolder.Documents.Child("Scores.txt")
		  
		  Dim input As TextInputStream
		  input = TextInputStream.Open(scoreFile, Xojo.Core.TextEncoding.UTF8)
		  
		  Dim tab As Text = &u09
		  Dim scoreLine As Text
		  Dim scores() As Text
		  Dim player As Integer
		  While Not input.EOF
		    scoreLine = input.ReadLine
		    scores = scoreLine.Split(tab)
		    player = Integer.FromText(scores(0))
		    For score As Integer = 1 To scores.Ubound - 1
		      PlayerScores(player, score - 1) = Integer.FromText(scores(score))
		    Next
		    player = player + 1
		  Wend
		  
		  input.Close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		PlayerScores(3,17) As Integer
	#tag EndProperty


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
